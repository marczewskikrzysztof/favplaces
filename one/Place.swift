//
//  Place.swift
//  one
//
//  Created by Krzysztof Marczewski on 09.01.2018.
//  Copyright © 2018 Veriqus. All rights reserved.
//

import UIKit

class Place {
    
    //MARK: Properties
    
    var name: String
    var photo: UIImage?
    
    init?(name: String, photo: UIImage?) {
        self.name = name
        self.photo = photo
    }
    
}
