//
//  PlaceTableViewController.swift
//  one
//
//  Created by Krzysztof Marczewski on 10.01.2018.
//  Copyright © 2018 Veriqus. All rights reserved.
//

import UIKit
import os.log

class PlaceTableViewController: UITableViewController {
    
    //MARK: Properties
    
    var places = [Place]()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadSamplePlaces()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PlaceTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PlaceTableViewCell else {
            fatalError("Dequeue cell error")
        }
        let place = places[indexPath.row]
        
        cell.nameLabel.text = place.name
        cell.photoImageView.image = place.photo

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.places[sourceIndexPath.row]
        places.remove(at: sourceIndexPath.row)
        places.insert(movedObject, at: destinationIndexPath.row)
        NSLog("%@", "\(sourceIndexPath.row) => \(destinationIndexPath.row) \(places)")
        // To check for correctness enable: self.tableView.reloadData()
    }

    
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
 

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
            
        case "AddItem":
            os_log("Adding a new place.", log: OSLog.default, type: .debug)
            
        case "ShowDetail":
            guard let placeDetailViewController = segue.destination as? PlaceViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedPlaceCell = sender as? PlaceTableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedPlaceCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedPlace = places[indexPath.row]
            placeDetailViewController.place = selectedPlace
            
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
    }
 

    
    
    //MARK: Private Methods
    
    private func loadSamplePlaces() {
        
        let photo1 = UIImage(named: "place1")
        let photo2 = UIImage(named: "place2")
        let photo3 = UIImage(named: "place3")
        
        guard let placeOne = Place(name: "Home", photo: photo1)
            else {
                fatalError("initialization error")
        }
        guard let placeTwo = Place(name: "Bridge Nation Crossfit", photo: photo3)
            else {
                fatalError("initialization error")
        }
        guard let placeThree = Place(name: "Colour of Ostrava", photo: photo2)
            else {
                fatalError("initialization error")
        }
        
        
        places += [placeOne, placeTwo, placeThree]
        
    }
    
    //MARK: Actions
    
    @IBAction func startEdit(_ sender: UIBarButtonItem) {
        self.isEditing = !self.isEditing
    }
    
    @IBAction func unwindToPlaceList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? PlaceViewController, let place = sourceViewController.place {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // Update place.
                places[selectedIndexPath.row] = place
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
            else {
                // Add a new place.
                let newIndexPath = IndexPath(row: places.count, section: 0)
                
                places.append(place)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
    }
}
